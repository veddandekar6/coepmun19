// ===== Scroll to Top ==== 
$(window).scroll(function() {
    if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200);    // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
    }
});
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});

$("#nav ul li a[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top
     }, 300, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

});

window.onload = function(){
    if ( window.innerWidth > 400 ) {
        document.querySelector('[href="tel:+919820966264"]').href = '';
        document.querySelector('[href="tel:+917588287141"]').href = '';
    }
};
// //GO TO TOP BUTTON
// jQuery(document).ready(function($){
//   // browser window scroll (in pixels) after which the "back to top" link is shown
//   var offset = 300,
//     //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
//     offset_opacity = 1200,
//     //duration of the top scrolling animation (in ms)
//     scroll_top_duration = 700,
//     //grab the "back to top" link
//     $back_to_top = $('.cd-top');

//   //hide or show the "back to top" link
//   $(window).scroll(function(){
//     ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
//     if( $(this).scrollTop() > offset_opacity ) { 
//       $back_to_top.addClass('cd-fade-out');
//     }
//   });

//   //smooth scroll to top
//   $back_to_top.on('click', function(event){
//     event.preventDefault();
//     $('body,html').animate({
//       scrollTop: 0 ,
//       }, scroll_top_duration
//     );
//   });

// });

// //OVERLAYS
// $(document).ready(function() {
//   if (Modernizr.touch) {
//     // show the close overlay button
//     $(".close-overlay").removeClass("hidden");
//     // handle the adding of hover class when clicked
//     $(".effects .img").click(function(e) {
//       e.preventDefault();
//       e.stopPropagation();
//       if (!$(this).hasClass("hover")) {
//         $(this).addClass("hover");
//       }
//     });
//     // handle the closing of the overlay
//     $(".close-overlay").click(function(e) {
//       e.preventDefault();
//       e.stopPropagation();
//       if ($(this).closest(".img").hasClass("hover")) {
//         $(this).closest(".img").removeClass("hover");
//       }
//     });
//   } else {
//     // handle the mouseenter functionality
//     $(".effects .img").mouseenter(function() {
//       $(this).addClass("hover");
//     })
//     // handle the mouseleave functionality
//     .mouseleave(function() {
//       $(this).removeClass("hover");
//     });
//   }
// });

// // SMOOTH NAV SCROLL 
// $(function() {
//   $('a[href*=#]:not([href=#])').click(function() {
//     if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

//       var target = $(this.hash);
//       target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
//       if (target.length) {
//         $('html,body').animate({
//           scrollTop: target.offset().top - 80
//         }, 2000);
//         return false;
//       }
//     }
//   });
// });

// // WAYPOINTS
// $(function() {

//   $('.wp1').waypoint(function() {
//     $('.wp1').addClass('animated bounceInLeft');
//   }, {
//     offset: '85%'
//   });

//   $('.wp2').waypoint(function() {
//     $('.wp2').addClass('animated bounceInRight');
//   }, {
//     offset: '85%'
//   });

//   $('.wp3').waypoint(function() {
//     $('.wp3').addClass('animated bounceInLeft');
//   }, {
//     offset: '85%'
//   });

//   $('.wp4').waypoint(function() {
//     $('.wp4').addClass('animated bounceInRight');
//   }, {
//     offset: '85%'
//   });

//   $('.wp5').waypoint(function() {
//     $('.wp5').addClass('animated bounceInLeft');
//   }, {
//     offset: '85%'
//   });

// });


// //NAVBAR
// (function($) {
//     "use strict"; // Start of use strict

//     // jQuery for page scrolling feature - requires jQuery Easing plugin
//     $(document).on('click', 'a.page-scroll', function(event) {
//         var $anchor = $(this);
//         $('html, body').stop().animate({
//             scrollTop: ($($anchor.attr('href')).offset().top - 80)
//         }, 1250, 'easeInOutExpo');
//         event.preventDefault();
//     });

//     // Highlight the top nav as scrolling occurs
//     $('body').scrollspy({
//         target: '#mainNav',
//         offset: 81
//     });

//     // Closes the Responsive Menu on Menu Item Click
//     $('.navbar-collapse ul li a').click(function() {
//         $('.navbar-toggle:visible').click();
//     });

//     // Offset for Main Navigation
//     $('#mainNav').affix({
//         offset: {
//             top: 100
//         }
//     })
// })(jQuery);

// //PRELOADER
// (function($) {

//   "use strict";
//    // Add the User Agent to the <html>
//    // will be used for IE10 detection (Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0))
//   var doc = document.documentElement;
//   doc.setAttribute('data-useragent', navigator.userAgent);
  
//   /* Preloader 
//    * -------------------------------------------------- */
//   var ssPreloader = function() {
//     $(window).on('load', function() {  
//       // force page scroll position to top at page refresh
//       $('html, body').animate({ scrollTop: 0 }, 'normal');
//         // will first fade out the loading animation 
//         $("#loader").fadeOut("slow", function(){
//           // will fade out the whole DIV that covers the website.
//           $("#preloader").delay(300).fadeOut("slow");
//         }); 
//       });
//   }; 

//   (function ssInit() {
//     ssPreloader();
//   })();
// })(jQuery);


// //SLIDESHOW
// (function() {
//     var quotes = $(".quotes");
//     var quoteIndex = -1;
    
//     function showNextQuote() {
//         ++quoteIndex;
//         if (quoteIndex == quotes.length) {
//           return $(".slideshow").fadeOut(800);
//         }
//         quotes.eq(quoteIndex % quotes.length)
//             .fadeIn(1000)
//             .delay(500)
//             .fadeOut(1000, showNextQuote);
//     }
    
//     showNextQuote();
    
// })();

var swiper = new Swiper('.blog-slider', {
      spaceBetween: 30,
      effect: 'fade',
      loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
      mousewheel: {
        invert: false,
      },
      // autoHeight: true,
      pagination: {
        el: '.blog-slider__pagination',
        clickable: true,
      }
    });

if (window.matchMedia('screen and (max-width: 768px)').matches) {     $('.navbar-collapse a').click(function (e) {
        $('.navbar-collapse').collapse('toggle');
      });
  }
 